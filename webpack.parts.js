const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');

exports.devServer = function({ host, port, path }) {
    return {
        devServer: {
            // Enable history API fallback so HTML5 History API based
            // routing works. This is a good default that will come
            // in handy in more complicated setups.
            historyApiFallback: true,
            // Don't refresh if hot loading fails. If you want
            // refresh behavior, set hot: true instead.
            hotOnly: true,
            // Error handling
            overlay : true,
            // Display only errors to reduce the amount of output.
            stats: 'errors-only',
            // Parse host and port from env to allow customization.
            //
            // If you use Vagrant or Cloud9, set
            // host: options.host || '0.0.0.0';
            //
            // 0.0.0.0 is available to all network devices
            // unlike default `localhost`.
            host: host, // Defaults to `localhost`
            port: port, // Defaults to 8080
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NamedModulesPlugin(),
        ],
        module: {
            loaders: [
                { test: require.resolve("react"), loader: "expose?React" }
                // { test: /\.jsx?$/, loaders: ['react-hot', 'jsx?harmony'], include: path }
            ]
        },
    }
};

exports.lintJavaScript = function({ include, exclude, options }) {
    return {
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: include,
                    exclude: exclude,
                    enforce: 'pre',
                    loader: 'eslint-loader',
                    options: options,
                },
            ],
        },
    };
}

exports.loadCSS = function({ include, exclude} = {}) {
    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    include: include,
                    exclude: exclude,
                    use: ['style-loader', 'css-loader', 'sass-loader'],
                },
            ],
        },
    };
};

exports.extractCSS = function({ include, exclude, use }) {
    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    include: include,
                    exclude: exclude,
                    use: ExtractTextPlugin.extract({
                        use: use,
                        fallback: 'style-loader'
                    }),
                },
            ],
        },
        plugins: [
            // Output extracted CSS to a file
            new ExtractTextPlugin('ui_src/[name].[contenthash:8].css'),
            new OptimizeCssAssetsPlugin()
        ],
    };
};

exports.purifyCSS = function({ paths }) {
    return {
        plugins: [
            new PurifyCSSPlugin({ paths: paths }),
        ],
    };
};

exports.loadImages = function({ include, exclude, options } = {}) {
    return {
        module: {
            rules: [
                {
                    test: /\.(png|jpg)$/,
                    include: include,
                    exclude: exclude,
                    use: {
                        loader: 'url-loader',
                        options: options,
                    },
                },
            ],
        },
    };
};

exports.loadFonts = function({ include, exclude, options } = {}) {
    return {
        module: {
            rules: [
                {
                    // Capture eot, ttf, svg, woff, and woff2
                    test: /\.(woff2?|ttf|svg|eot)(\?v=\d+\.\d+\.\d+)?$/,
                    include: include,
                    exclude: exclude,
                    use: {
                        loader: 'file-loader',
                        options: options,
                    },
                },
            ],
        },
    };
};

exports.generateSourceMaps = function({ type }) {
    return {
        devtool: type,
    };
};

exports.extractBundles = ({ bundles} ) => ({
    plugins: bundles.map((bundle) => (
        new webpack.optimize.CommonsChunkPlugin(bundle)
    )),
});

exports.loadJavaScript = function({ include, exclude }) {
    return {
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    include: include,
                    exclude: exclude,
                    loader: 'babel-loader',
                    options: {
                        // Enable caching for improved performance during
                        // development.
                        // It uses default OS directory by default. If you need
                        // something more custom, pass a path to it.
                        // I.e., { cacheDirectory: '<path>' }
                        cacheDirectory: true,
                    },
                },
            ],
        },
    };
};

exports.clean = function(path) {
    return {
        plugins: [
            new CleanWebpackPlugin([path]),
        ],
    };
};

// exports.minifyJavaScript = function({ useSourceMap }) {
//     return {
//         plugins: [
//             new webpack.optimize.UglifyJsPlugin({
//                 sourceMap: useSourceMap,

//                 // Don't beautify output (enable for neater output)
//                 beautify: false,

//                 // Eliminate comments
//                 comments: false,

//                 compress: {
//                     warnings: false,

//                     // Drop `console` statements
//                     drop_console: true
//                 },

//                 // Mangling specific options
//                 mangle: {

//                     // Don't mangle $
//                     except: ['$'],

//                     // Don't care about IE8
//                     screw_ie8 : true,

//                     // Don't mangle function names
//                     keep_fnames: true,
//                 }
//             }),
//         ],
//     };
// };

exports.setFreeVariable = function(key, value) {
    const env = {};
    env[key] = JSON.stringify(value);
    return {
        plugins: [
            new webpack.DefinePlugin(env),
        ],
    };
};

// <meta name="viewport" content="width=device-width, initial-scale=1.0">
// Creating the index file
// https://www.npmjs.com/package/html-webpack-template
exports.indexTemplate = function(options) {
  return {
    plugins: [
      new HtmlWebpackPlugin({
        template: require('html-webpack-template'),
        title: options.title,
        appMountId: options.appMountId,
        inject: false,
        mobile: true,
        baseHref: './',
        meta: [
            {
                'name': 'format-detection',
                content: 'telephone=no'
            }
        ],
        links: [
            {
                href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
                rel: 'stylesheet'
            },
          //   {
          //       href: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css',
          //       rel: 'stylesheet'
          //   },

          {
            href: '/favicon/apple-icon-57x57.png',
            rel: 'apple-touch-icon',
            sizes: '57x57',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-60x60.png',
            rel: 'apple-touch-icon',
            sizes: '60x60',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-72x72.png',
            rel: 'apple-touch-icon',
            sizes: '72x72',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-76x76.png',
            rel: 'apple-touch-icon',
            sizes: '76x76',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-114x114.png',
            rel: 'apple-touch-icon',
            sizes: '114x114',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-120x120.png',
            rel: 'apple-touch-icon',
            sizes: '120x120',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-144x144.png',
            rel: 'apple-touch-icon',
            sizes: '144x144',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-152x152.png',
            rel: 'apple-touch-icon',
            sizes: '152x152',
            // type: 'image/png'
          },
          {
            href: '/favicon/apple-icon-180x180.png',
            rel: 'apple-touch-icon',
            sizes: '180x180',
            // type: 'image/png'
          },

          {
            href: '/favicon/android-icon-36x36.png',
            rel: 'icon',
            sizes: '36x36',
            type: 'image/png'
          },
          {
            href: '/favicon/android-icon-48x48.png',
            rel: 'icon',
            sizes: '48x48',
            type: 'image/png'
          },
          {
            href: '/favicon/android-icon-72x72.png',
            rel: 'icon',
            sizes: '72x72',
            type: 'image/png'
          },
          {
            href: '/favicon/android-icon-96x96.png',
            rel: 'icon',
            sizes: '96x96',
            type: 'image/png'
          },
          {
            href: '/favicon/android-icon-144x144.png',
            rel: 'icon',
            sizes: '144x144',
            type: 'image/png'
          },
          {
            href: '/favicon/android-icon-192x192.png',
            rel: 'icon',
            sizes: '192x192',
            type: 'image/png'
          },

          {
            href: '/favicon/favicon-32x32.png',
            rel: 'icon',
            sizes: '32x32',
            type: 'image/png'
          },
          {
            href: '/favicon/favicon-96x96.png',
            rel: 'icon',
            sizes: '96x96',
            type: 'image/png'
          },
          {
            href: '/favicon/favicon-16x16.png',
            rel: 'icon',
            sizes: '16x16',
            type: 'image/png'
          },
          {
            href: '/favicon/manifest.json',
            rel: 'manifest'
          },

        ]
      })
    ]
  };
}

exports.globalInclude = function() {
    return {
        plugins: [
            new webpack.ProvidePlugin({
                React: "react",
                ReactDOM: "react-dom",
                $: "jquery",
                uuid: "uuid/v1",
                moment: "moment",
                _: "lodash"
            }),
            // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        ]
    };
};

exports.dontParse = function(options) {
    const alias = {};
    alias[options.name] = options.path;
    return {
        module: {
            noParse: [
                new RegExp(options.path),
            ],
        },
        resolve: {
            alias: alias,
        },
    };
};

exports.minifyJavaScript = () => ({
    plugins: [
        new BabiliPlugin(),
    ],
});
