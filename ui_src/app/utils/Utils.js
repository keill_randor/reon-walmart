export default {
    price(val) {
    	if (val)
    		return val.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')

    	return '0.00';
    }
}