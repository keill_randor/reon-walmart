/* REQUESET */
import request from 'superagent/lib/client';
import Promise from 'es6-promise';

export default {

    encodeParams: (params ) => {
        var outparams   = [];
        var queryString = '?';
        for (var p in params){ outparams.push( p + '=' + params[p] ); }
        return  queryString += outparams.join('&');
    },

    GET: (url, params ) => {

        var host = '';

        var promise = new Promise((resolve, reject) => {
            request
                .get(host + '/api/' + url + (params ? this.encodeParams(params) : ''))
                .end((err, response) => {
                    if (err) reject(err);
                    // console.log('response on GET' , response)
                    resolve(JSON.parse(response.text));
                });
        });

        return promise;
    },

    POST:(url, formData) => {

        var host = '';

        var promise = new Promise((resolve, reject) => {
            $.ajax({
                url: host + url,
                type:'POST',
                dataType: false,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(formData),
                success: function(data) {
                    resolve(data);
                },
                error:function(data) {
                    reject(data);
                },
            });
        });

        return promise;
    },
};
