const DiscountsReducer = (discounts = [], action) => {

	switch (action.type)
	{
		case 'ADD_DISCOUNT':
			discounts.push(action.data);
			return discounts;

		case 'DEL_DISCOUNT':
			discounts = discounts.filter(x => {
				if (x.code !== action.data)
					return x;
			})
			return discounts;
	}

	return discounts;
};

export default DiscountsReducer;
