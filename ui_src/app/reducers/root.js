import CartReducer from './Cart.reducer';
import DeliveryReducer from './Delivery.reducer';
import DiscountReducer from './Discount.reducer';

const rootReducer = (state, action) => {

	return Object.assign({}, state, {
		cart: CartReducer(state.cart, action),
		delivery: DeliveryReducer(state.delivery, action),
		discount: DiscountReducer(state.discount, action),
	});

};

export default rootReducer;