const DeliveryReducer = (delivery = {}, action) => {

	switch (action.type)
	{
		case 'SET_DELIVERY_TYPE':
			delivery.type = action.data.toLowerCase();
			return delivery;
	}

	return delivery;
};

export default DeliveryReducer;
