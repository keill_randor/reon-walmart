export const addDiscount = (discount) => ({
	type: 'ADD_DISCOUNT', data : discount
});

export const delDiscount = (discount) => ({
	type: 'DEL_DISCOUNT', data : discount
});