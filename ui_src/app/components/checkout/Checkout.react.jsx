import { setDeliveryType } from '../../actions/Delivery.actions';
import { delDiscount } from '../../actions/Discounts.actions';

import Utils from '../../utils/Utils';

import SeeItemDetails from './SeeItemDetails.react';
import ApplyPromo from './ApplyPromo.react';
import PickupSavings from './PickupSavings.react';

export default class Checkout extends React.Component {

    constructor(props) {
        super(props);

        this.getItemPrice = this.getItemPrice.bind(this);
    }

    getItemPrice(itemId) {
        var itemData = this.props.store.getState().itemData;

        return itemData.filter(x => {
            if (x.itemId === itemId)
                return x;
        })[0].price[0].val
    }

    render() {
        var store = this.props.store,
            state = store.getState(),
            cart = state.cart,
            itemData = state.itemData,
            discount = state.discount;

        var subtotal = 0,
            pickupSavings = 0;

        for (var i=0; i<cart.items.length; i++)
            subtotal += this.getItemPrice(cart.items[i].itemId);

        if (state.delivery.type === 'pickup')
            subtotal -= state.delivery.pickupSavings;


        for (var i=0; i<discount.length; i++)
            if (discount[i].type === 'percent')
                subtotal = (1-discount[i].amount) * subtotal;

        var tax = subtotal * state.tax;
        var total = subtotal + tax;

        return (
            <div className='checkout'>

                <div className='checkout__title'>
                    Checkout
                </div>

                <div className='checkout__hr' />

                { discount.map(x => {
                    return (
                        <CheckoutRow
                            title={x.title}
                            body={
                                <span onClick={() => {
                                    store.dispatch(delDiscount(x.code));
                                }}>
                                    <i className='fa fa-minus' />
                                </span>
                            }
                            key={'discount'+x.code} />
                    )
                })}

                <CheckoutRow
                    title='Subtotal'
                    body={'$'+Utils.price(subtotal)} />

                <div className='checkout__row'>
                    <select
                        val={state.delivery.type}
                        onChange={e => {
                            store.dispatch(setDeliveryType(e.target.value));
                        }}>

                        <option val='delivery'>Delivery</option>
                        <option val='pickup'>Pickup</option>
                    </select>
                </div>

                { state.delivery.type === 'pickup' ?
                    <CheckoutRow
                        title={<PickupSavings />}
                        body={'$'+Utils.price(state.delivery.pickupSavings)}
                        style={{
                            body : {
                                'color' : '#FF0000'
                            }
                        }} />
                :
                    <CheckoutRow
                        title='Delivery'
                        body={state.delivery.deliveryPrice === 0 ? 'Free' : Utils.price(state.delivery.deliveryPrice)} />
                }

                <CheckoutRow
                    title='Est Taxes & Fees'
                    body={'$'+Utils.price(tax)} />

                <div className='checkout__hr' />

                <div className='checkout__total'>
                    <h1>Est.total</h1>
                    <h2>${Utils.price(total)}</h2>
                </div>

                <div className='checkout__hr' />

                <SeeItemDetails
                    state={state} />

                <div className='checkout__hr' />

                <ApplyPromo
                    state={state} />

            </div>
        )
    }
}

class CheckoutRow extends React.Component {

    render() {
        var style = this.props.style || { body : {}, title : {}};

        return (
            <div className='checkout__row'>
                <h1 style={style.title || {}}>
                    {this.props.title}
                </h1>
                { this.props.body &&
                    <h2 style={style.body || {}}>
                        {this.props.body}
                    </h2>
                }
            </div>
        )
    }
}