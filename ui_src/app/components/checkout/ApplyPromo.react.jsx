import store from '../../stores/Store';

import { addDiscount } from '../../actions/Discounts.actions';

export default class ApplyPromo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            visible : false,
            code : ''
        };

        this.apply = this.apply.bind(this);
    }

    apply() {
        var discount = this.props.state.discount,
            code = this.state.code;

        for (var i=0; i<discount.length; i++) {
            if (discount[i].code.toLowerCase() === code.toLowerCase()) {
                alert('Discount already applied');
                return;
            }
        }

        var match = true;

        switch (this.state.code.toLowerCase())
        {
            case 'discount':
                store.dispatch(addDiscount({
                    title : 'DISCOUNT 10%',
                    code : 'DISCOUNT',
                    type : 'percent',
                    amount : 0.1,
                    amountStr : '10%'
                }));

                break;

            default:
                alert('No discount found');
                match = false;
        }

        if (match)
            this.setState({
                code : '',
                visible : false
            });
    }

    render() {
        return ([
            <div
                className='item-details checkout__row'
                onClick={() => { this.setState({ visible : !this.state.visible}, () => {
                    $('#promo').focus();
                }) }}>

                <h1>Apply promo code</h1>

                <div className='item-details__icon'>
                    <i className={'fa fa-'+(this.state.visible ? 'minus' : 'plus')} />
                </div>
            </div>,

            ( this.state.visible &&
                <div className='apply-promo'>
                    <input
                        type='text'
                        id='promo'
                        value={this.state.code}
                        placeholder='Promo Code'
                        onChange={e => { this.setState({ code : e.target.value }); }}
                        onKeyDown={e => {
                            if (e.which === 13)
                                this.apply();
                        }} />

                    <div
                        className='apply-promo__btn'
                        onClick={this.apply}>

                        Apply
                    </div>
                </div>
            )
        ])
    }
}
