import Utils from '../../utils/Utils';

export default class SeeItemDetails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            visible : false
        };
    }

    render() {
        var state = this.props.state;

        return ([
            <div
                className='item-details checkout__row'
                onClick={() => { this.setState({ visible : !this.state.visible}) }}>

                <h1>See item details</h1>

                <div className='item-details__icon'>
                    <i className={'fa fa-'+(this.state.visible ? 'minus' : 'plus')} />
                </div>
            </div>,

            ( this.state.visible && state.cart.items.map(x => {
                var itemData = state.itemData.filter(y => {
                    if (x.itemId === y.itemId)
                        return y;
                })[0];

                if (!itemData)
                    return null;

                return (

                    <div className='item-details__item'>
                        <img src={itemData.image} />

                        <div className='item-details__item--details'>
                            {itemData.title}
                            <br />
                            <b>Qty:</b> {x.qty}
                            <br />
                            <b>Price:</b>&nbsp;
                            { itemData.price.map((z, i) => {
                                return (
                                    <span
                                        className={'item-details__item--'+(i > 0 ? 'old' : 'price')}
                                        key={'itemData'+z.timestamp}>

                                        ${Utils.price(z.val)}
                                    </span>
                                )
                            })}
                        </div>
                    </div>
                )
            }))
        ])
    }
}
