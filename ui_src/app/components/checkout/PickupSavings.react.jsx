export default class PickupSavings extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            popup : false
        };
    }

    render() {

        return ([
            <span
                className='pickup-savings'
                onClick={() => { this.setState({ popup : !this.state.popup }) }}>

                <u>Pickup Savings</u>
            </span>,

            ( this.state.popup &&
                <div className='pickup-savings__popup'>
                    Picking up your order in the store helps cut costs, and we pass the savings to you.
                </div>
            )
        ])
    }
}