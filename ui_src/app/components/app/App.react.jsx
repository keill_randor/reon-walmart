import store from '../../stores/Store';

import Checkout from '../checkout/Checkout.react';

export default class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            store : store
        };

        this.onStoreChange = this.onStoreChange.bind(this);
    }

    componentWillMount() {
        store.subscribe(this.onStoreChange)
    }

    onStoreChange() {
        this.setState({
            store : store
        })
    }

    render() {
        return (
            <div className='app'>
                <Checkout
                    store={this.state.store} />
            </div>
        )
    }
}