import App from './components/app/App.react';

import { AppContainer } from 'react-hot-loader';

const render = App => {
    ReactDOM.render(
        <AppContainer>
            <App />
        </AppContainer>,

        document.getElementById('template')
    );
};

render(App);

if (module.hot) {
    module.hot.accept('./components/app/App.react', () => render(App));
}

// import component from './components/app/App.react';
// document.body.appendChild(component());
