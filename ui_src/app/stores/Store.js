import { createStore } from 'redux';
import rootReducer from '../reducers/root';

const initialState = {
	cart: {
		items : [
			{
				itemId: 1,
				qty: 1,
				pickup: false,
			}
		]
	},

	itemData: [
		{
			itemId : 1,
			title : 'OFM Essentials Racecar-Style Leather Gaming Chair, Multiple Colors',
			price : [
				{
					val : 95,
					timestamp : 1538006069
				},
				{
					val : 103.24,
					timestamp : 1538005069
				}
			],
			image : 'https://i5.walmartimages.com/asr/66fae1aa-d8bc-4089-8554-c1b1fc0c6d92_1.a18ee650ed91efecab34d94b75a5ba0a.jpeg',
		}
	],

	tax : 0.095,

	delivery : {
		type : 'delivery',
		deliveryPrice : 0,
		pickupSavings : 3.95,
	},

	discount : [
	],
};

const store = createStore(rootReducer, initialState);

window.store = store;

export default store;
